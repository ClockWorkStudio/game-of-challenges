import { clamp } from 'lodash';
import { Character, CharacterState, Effect, EffectKind } from './types';

class CharacterOnBoard {
  profile: Character;

  state: CharacterState;

  constructor(profile: Character) {
    this.profile = profile;
    this.state = new Map<EffectKind, number>();
  }

  getState = (kind: EffectKind): number => this.state.get(kind) ?? 0;

  isDead = (): boolean => this.getState('dead') > 0;

  isActive = (): boolean => !this.isDead();

  isTarget = (): boolean => !this.isDead();

  takeEffect = (kind: EffectKind, amount: number): Effect => {
    const targetState = this.state;
    const oldAmount = targetState.get(kind) ?? 0;
    const newAmount = oldAmount + amount;
    targetState.set(kind, newAmount);
    return {
      target: this.profile,
      kind,
      amount,
    };
  };

  takeDamage = (amount: number): Effect[] => {
    const effects: Effect[] = [];
    effects.push(this.takeEffect('damage', amount));
    if (this.hp <= 0) {
      effects.push(this.takeEffect('dead', 1));
    }
    return effects;
  };

  get hp(): number {
    const result = this.profile.maxHp - this.getState('damage');
    return clamp(result, 0, this.profile.maxHp);
  }
}
export default CharacterOnBoard;
