export type CharacterSide = 'heroes' | 'enemies' | 'others';
export type Character = {
  maxHp: number;
  attack: number;
  name: string;
  side: CharacterSide;
  skills: Array<SkillName>;
};
export type CharacterState = Map<EffectKind, number>;
export type WinLose = 'win' | 'lose' | 'timeout';
export type Result = {
  id?: number;
  challengeId: number;
  characters: Character[];
  winLose: WinLose;
  turns: Array<Turn>;
};
export type Turn = {
  actions: Array<Action>;
  snapshot: CharacterState[];
};
export type ActionKind = 'attack' | 'win' | 'lose' | SkillName;
export type Action = {
  subject: Character;
  kind: ActionKind;
  effects: Array<Effect>;
};
export type EffectKind = 'damage' | 'dead';
export type Effect = { target: Character; kind: EffectKind; amount: number };
export type SkillName = 'magicMissile';
