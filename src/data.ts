import { Character, SkillName } from './battle/types';

export type Challenge = { id: number; name: string; enemies: Character[] };
export type Organization = { id: number; name: string; heroes: Character[] };
export type Skill = { id: number; name: SkillName };
export type Data = {
  challenges: Challenge[];
  organizations: Organization[];
  skills: Skill[];
};
const data: Data = {
  challenges: [
    {
      id: 1,
      name: '1F: 最初の試練',
      enemies: [
        {
          maxHp: 8,
          attack: 5,
          name: 'スライムA',
          side: 'enemies',
          skills: [],
        },
        {
          maxHp: 8,
          attack: 5,
          name: 'スライムB',
          side: 'enemies',
          skills: [],
        },
        {
          maxHp: 8,
          attack: 5,
          name: 'スライムC',
          side: 'enemies',
          skills: [],
        },
        {
          maxHp: 8,
          attack: 5,
          name: 'スライムD',
          side: 'enemies',
          skills: [],
        },
      ],
    },
    {
      id: 99,
      name: '99F: 最後の試練',
      enemies: [
        {
          maxHp: 9999,
          attack: 9999,
          name: '大魔王',
          side: 'enemies',
          skills: [],
        },
      ],
    },
  ],
  organizations: [
    {
      id: 1,
      name: '編成1',
      heroes: [
        {
          maxHp: 15,
          attack: 10,
          name: 'アリス',
          side: 'heroes',
          skills: ['magicMissile'],
        },
        {
          maxHp: 20,
          attack: 10,
          name: 'ボブ',
          side: 'heroes',
          skills: [],
        },
      ],
    },
    {
      id: 2,
      name: '編成2',
      heroes: [
        {
          maxHp: 15,
          attack: 10,
          name: 'アリス',
          side: 'heroes',
          skills: [],
        },
        {
          maxHp: 20,
          attack: 10,
          name: 'ボブ',
          side: 'heroes',
          skills: [],
        },
        {
          maxHp: 15,
          attack: 10,
          name: 'キャロル',
          side: 'heroes',
          skills: [],
        },
        {
          maxHp: 20,
          attack: 10,
          name: 'デビッド',
          side: 'heroes',
          skills: [],
        },
      ],
    },
  ],
  skills: [{ id: 1, name: 'magicMissile' }],
};

export const getChallenge = (id: number): Challenge => {
  const challenge = data.challenges.find((c) => c.id === id);
  if (challenge) {
    return challenge;
  }
  throw Error('挑戦相手が見つかりません');
};

export const getOrganization = (id: number): Organization => {
  const organization = data.organizations.find((o) => o.id === id);
  if (organization) {
    return organization;
  }
  throw Error('編成が見つかりません');
};

export default data;
