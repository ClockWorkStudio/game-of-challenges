import { Dexie } from 'dexie';
import { ActionKind, CharacterSide, EffectKind, WinLose } from './battle/types';

export interface Result {
  id?: number;
  challengeId: number;
  winLose: WinLose;
}

export interface Character {
  id?: number;
  resultId: number;
  order: number;
  maxHp: number;
  attack: number;
  name: string;
  side: CharacterSide;
}

export interface Turn {
  id?: number;
  resultId: number;
  order: number;
}

export interface CharacterState {
  id?: number;
  turnId: number;
  characterId: number;
  kind: EffectKind;
  amount: number;
}

export interface Action {
  id?: number;
  turnId: number;
  order: number;
  subjectOrder: number;
  kind: ActionKind;
}

export interface Effect {
  id?: number;
  actionId: number;
  order: number;
  targetOrder: number;
  kind: EffectKind;
  amount: number;
}

export interface Acquisition {
  id?: number;
  characterId: number;
  skillId: number;
}

export class MySubClassedDexie extends Dexie {
  results!: Dexie.Table<Result>;

  characters!: Dexie.Table<Character>;

  turns!: Dexie.Table<Turn>;

  characterStates!: Dexie.Table<CharacterState>;

  actions!: Dexie.Table<Action>;

  effects!: Dexie.Table<Effect>;

  acquisitions!: Dexie.Table<Acquisition>;

  constructor() {
    super('gameOfChallenges');
    this.version(1).stores({
      results: '++id, challengeId',
      characters: '++id, resultId',
      characterStates: '++id, turnId, characterId, kind',
      turns: '++id, resultId',
      actions: '++id, turnId',
      effects: '++id, actionId',
      acquisitions: '++id, characterId, skillId',
    });
  }
}

const db = new MySubClassedDexie();
export default db;
