import { random, sample, shuffle } from 'lodash';
import CharacterOnBoard from './battle/characterOnBoard';
import {
  Result,
  Turn,
  WinLose,
  Effect,
  Action,
  ActionKind,
} from './battle/types';
import { getChallenge, getOrganization } from './data';

const battle = (challengeId: number, organizationId: number): Result => {
  const characters: CharacterOnBoard[] = [];
  getOrganization(organizationId).heroes.forEach((h) => {
    characters.push(new CharacterOnBoard(h));
  });
  getChallenge(challengeId).enemies.forEach((e) => {
    characters.push(new CharacterOnBoard(e));
  });
  const judge = new CharacterOnBoard({
    maxHp: 1,
    attack: 1,
    name: '審判',
    side: 'others',
    skills: [],
  });
  characters.push(judge);
  const heroes = characters.filter((c) => c.profile.side === 'heroes');
  const enemies = characters.filter((c) => c.profile.side === 'enemies');
  const maxTurns = 10;
  const turns: Array<Turn> = [];
  let winLose: WinLose = 'timeout';

  function sampleOrThrow<T>(array: Array<T>): T {
    const choice = sample(array);
    if (choice) {
      return choice;
    }
    throw Error('対象の候補がいない');
  }

  const decideAction = (subject: CharacterOnBoard): ActionKind => {
    if (subject.profile.skills.includes('magicMissile')) {
      return 'magicMissile';
    }
    return 'attack';
  };

  const decideTarget = (subject: CharacterOnBoard): CharacterOnBoard => {
    if (heroes.includes(subject)) {
      return sampleOrThrow(enemies.filter((e) => e.isTarget()));
    }
    if (enemies.includes(subject)) {
      return sampleOrThrow(heroes.filter((e) => e.isTarget()));
    }
    throw Error('ターゲットを決定できない');
  };

  const doAttack = (subject: CharacterOnBoard): Effect[] => {
    const target = decideTarget(subject);
    const damageAmount = random(subject.profile.attack);
    return target.takeDamage(damageAmount);
  };

  const doMagicMissile = (subject: CharacterOnBoard): Effect[] => {
    const target = decideTarget(subject);
    const damageAmount = 20;
    return target.takeDamage(damageAmount);
  };

  const actionFuncs = new Map<
    ActionKind,
    (subject: CharacterOnBoard) => Effect[]
  >([
    ['attack', doAttack],
    ['magicMissile', doMagicMissile],
  ]);

  const doAction = (subject: CharacterOnBoard): Action => {
    const actionKind = decideAction(subject);
    const actionFunc = actionFuncs.get(actionKind);
    if (actionFunc) {
      return {
        subject: subject.profile,
        kind: actionKind,
        effects: actionFunc(subject),
      };
    }
    throw Error('行動が実行できません');
  };

  const doJudgment = (): Action[] => {
    const judgments: Action[] = [];
    // TODO: 判定時に敵味方両方全滅してたらどうするか考える
    const heroesLost = heroes.every((character) => character.isDead());
    if (heroesLost) {
      judgments.push({
        subject: judge.profile,
        kind: 'lose',
        effects: [],
      });
    }
    const heroesWin = enemies.every((character) => character.isDead());
    if (heroesWin) {
      judgments.push({
        subject: judge.profile,
        kind: 'win',
        effects: [],
      });
    }
    return judgments;
  };

  const takeSnapshot = () => characters.map((c) => new Map(c.state));

  [...Array(maxTurns)].some(() => {
    const actions: Array<Action> = [];
    turns.push({ actions, snapshot: takeSnapshot() });

    return shuffle(heroes.concat(enemies)).some((subject) => {
      if (!subject.isActive()) {
        return false;
      }
      actions.push(doAction(subject));
      const judgments = doJudgment();
      judgments.forEach((judgment) => {
        actions.push(judgment);
        if (judgment.kind === 'win' || judgment.kind === 'lose') {
          winLose = judgment.kind;
        }
      });

      return ['win', 'lose'].includes(winLose);
    });
  });
  return {
    challengeId,
    winLose,
    turns,
    characters: characters.map((character) => character.profile),
  };
};

export default battle;
