import { render } from 'react-dom';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React from 'react';
import App from './App';
import Challenges from './routes/challenges';
import Challenge from './routes/challenge';
import Top from './routes/top';
import Result from './routes/result';

const rootElement = document.getElementById('root');
render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<App />}>
        <Route index element={<Top />} />
        <Route path="challenges">
          <Route index element={<Challenges />} />
          <Route path=":id" element={<Challenge />} />
        </Route>
        <Route path="results">
          <Route path=":id" element={<Result />} />
        </Route>
        <Route
          path="*"
          element={
            <main style={{ padding: '1rem' }}>
              <p>データがありません</p>
            </main>
          }
        />{' '}
      </Route>
    </Routes>
  </BrowserRouter>,
  rootElement
);
