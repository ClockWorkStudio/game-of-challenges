import { useLiveQuery } from 'dexie-react-hooks';
import React from 'react';
import { Link } from 'react-router-dom';
import db, { Result } from '../db';

const linkToResult = (result: Result) => {
  const winLoseToShow = (() => {
    switch (result.winLose) {
      case 'win':
        return '勝利';
      case 'lose':
        return '敗北';
      case 'timeout':
        return '撤退';
      default:
        return '';
    }
  })();
  return (
    <Link to={`/results/${result.id}`}>
      挑戦{result.id}: {winLoseToShow}
    </Link>
  );
};

export default function Top() {
  const results = useLiveQuery(() =>
    db.results.orderBy('id').reverse().toArray()
  );
  return (
    <>
      <h2>挑戦の記録</h2>
      <ul>
        {results && results.map((result) => <li>{linkToResult(result)}</li>)}
      </ul>
    </>
  );
}
