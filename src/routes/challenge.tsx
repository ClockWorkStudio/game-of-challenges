import React from 'react';
import { useForm } from 'react-hook-form';
import { useParams, useNavigate } from 'react-router-dom';
import battle from '../battle';
import data, { getChallenge } from '../data';
import saveBattleResult from '../db/saveBattleResult';

export default function Challenge() {
  const params = useParams();
  const navigate = useNavigate();
  const { register, watch } = useForm();

  async function addResult(challengeId: number, organizationId: number) {
    return saveBattleResult(battle(challengeId, organizationId));
  }

  if (params.id) {
    const challengeId = Number(params.id);
    const challenge = getChallenge(challengeId);
    if (challenge) {
      return (
        <>
          <h2>{challenge.name}に挑戦</h2>
          <select {...register('organizationId')}>
            {data.organizations.map((o) => (
              <option value={o.id}>{o.name}</option>
            ))}
          </select>
          <button
            type="button"
            onClick={() => {
              addResult(challengeId, Number(watch('organizationId'))).then(
                (id) => {
                  navigate(`/results/${id}`);
                }
              );
            }}
          >
            はじめる
          </button>
        </>
      );
    }
  }

  navigate('/');
  return <>Loading...</>;
}
