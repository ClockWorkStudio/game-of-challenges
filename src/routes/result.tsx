import { useParams } from 'react-router-dom';
import { useLiveQuery } from 'dexie-react-hooks';
import { max, zip } from 'lodash';
import React from 'react';
import { getChallenge } from '../data';
import loadBattleResult from '../db/loadBattleResult';
import { Character, Effect, Turn } from '../battle/types';

export default function Challenge(): JSX.Element {
  const params = useParams();
  const resultId = Number(params.id);

  const result = useLiveQuery(() => loadBattleResult(resultId));

  const createEffectText = (effect: Effect): string => {
    switch (effect.kind) {
      case 'damage':
        return `${effect.target.name}は${effect.amount}のダメージを受けた`;
      case 'dead':
        return `${effect.target.name}は倒れた`;
      default:
        return '???';
    }
  };

  if (result) {
    const challenge = getChallenge(result.challengeId);
    if (challenge) {
      const sayAboutBoard = (turn: Turn) => {
        const { characters } = result;
        const heroes = characters.filter((c) => c.side === 'heroes');
        const enemies = characters.filter((c) => c.side === 'enemies');
        const hpOf = (character: Character): number => {
          const characterAndState = zip(characters, turn.snapshot).find(
            ([c, _]) => c === character
          );
          const state = characterAndState ? characterAndState[1] : undefined;
          if (state) {
            const damage = state.get('damage') ?? 0;
            return max([character.maxHp - damage, 0]) ?? 0;
          }
          throw Error('ダメージ情報が取得できません');
        };
        const textOf = (character: Character) => (
          <span>
            {character.name}
            {character.skills.includes('magicMissile') ? '(ミ)' : ''}:{' '}
            {hpOf(character)}/{character.maxHp}
          </span>
        );
        return (
          <>
            <li>{heroes.map(textOf)}</li>
            <li>{enemies.map(textOf)}</li>
          </>
        );
      };
      return (
        <>
          <h2>挑戦結果 {result.id}</h2>
          <p>{challenge.name}と戦いました</p>
          <p>勝敗：{result.winLose}</p>
          <p>ターン数：{result.turns.length}</p>
          <ul>
            {result.turns.map((turn, index) => (
              <>
                <li>{index + 1}ターン目</li>
                {sayAboutBoard(turn)}
                <ol>
                  {turn.actions.map((action) => {
                    switch (action.kind) {
                      case 'attack':
                        return (
                          <>
                            <li>{action.subject.name}が攻撃した</li>
                            <ol>
                              {action.effects.map((effect) => (
                                <li>{createEffectText(effect)}</li>
                              ))}
                            </ol>
                          </>
                        );
                      case 'magicMissile':
                        return (
                          <>
                            <li>
                              {action.subject.name}がマジックミサイルを放った
                            </li>
                            <ol>
                              {action.effects.map((effect) => (
                                <li>{createEffectText(effect)}</li>
                              ))}
                            </ol>
                          </>
                        );
                      case 'win':
                        return <li>勝った</li>;
                      case 'lose':
                        return <li>負けた</li>;
                      default:
                        return <li>???</li>;
                    }
                  })}
                </ol>
              </>
            ))}
          </ul>
        </>
      );
    }
  }

  return <>Loading...</>;
}
