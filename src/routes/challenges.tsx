import React from 'react';
import { Link } from 'react-router-dom';
import data from '../data';

export default function Challenges() {
  return (
    <main style={{ padding: '1rem 0' }}>
      <h2>挑戦相手選択</h2>
      <nav
        style={{
          borderRight: 'solid 1px',
          padding: '1rem',
        }}
      >
        {data.challenges.map((challenge) => (
          <Link
            style={{ display: 'block', margin: '1rem 0' }}
            to={`/challenges/${challenge.id}`}
            key={challenge.id}
          >
            {challenge.name}
          </Link>
        ))}
      </nav>
    </main>
  );
}
