import { zip } from 'lodash';
import {
  Result,
  Turn as BattleTurn,
  Action as BattleAction,
  Character as BattleCharacter,
} from '../battle/types';
import data from '../data';
import db, { Acquisition, Action, CharacterState } from '../db';

const saveBattleResult = async (result: Result) => {
  const resultId = Number(await db.results.add(result));

  const characterIds: number[] = await db.characters.bulkAdd(
    result.characters.map((character, index) => ({
      ...character,
      resultId,
      order: index + 1,
    })),
    { allKeys: true }
  );
  type SavedCharacter = { id: number; character: BattleCharacter };
  const savedCharacters: SavedCharacter[] = characterIds.map(
    (characterId, index) => ({
      id: Number(characterId),
      character: result.characters[index],
    })
  );

  const acquisitions: Acquisition[] = savedCharacters.flatMap((saved) =>
    saved.character.skills.map((skillName) => {
      const skill = data.skills.find((s) => s.name === skillName);
      if (skill) {
        return { characterId: saved.id, skillId: skill.id };
      }
      throw Error('スキルが見つかりません');
    })
  );
  await db.acquisitions.bulkAdd(acquisitions);

  type SavedTurn = { id: number; turn: BattleTurn };
  const turns = result.turns.map((turn, index) => ({
    ...turn,
    resultId,
    order: index + 1,
  }));
  const turnIds = await db.turns.bulkAdd(turns, { allKeys: true });
  const savedTurns: SavedTurn[] = turnIds.map((turnId, index) => ({
    id: Number(turnId),
    turn: result.turns[index],
  }));

  const characterStates: CharacterState[] = savedTurns.flatMap((t) =>
    zip(characterIds, [...t.turn.snapshot]).flatMap(([characterId, state]) => {
      if (characterId && state) {
        return [...state].map(([kind, amount]) => ({
          turnId: t.id,
          characterId, // character.id
          kind,
          amount,
        }));
      }
      throw Error('キャラクターの状態が正しく記録されていません');
    })
  );
  await db.characterStates.bulkAdd(characterStates);

  type ActionPair = { onBattle: BattleAction; onDb: Action };
  const actionPairs: ActionPair[] = savedTurns.flatMap((savedTurn) =>
    savedTurn.turn.actions.map((action, index) => {
      const onBattle = action;
      const subjectOrder = result.characters.indexOf(action.subject) + 1;
      const onDb = {
        ...action,
        turnId: savedTurn.id,
        order: index + 1,
        subjectOrder,
      };
      return { onBattle, onDb };
    })
  );
  const actionIds = await db.actions.bulkAdd(
    actionPairs.map((p) => p.onDb),
    { allKeys: true }
  );
  type SavedAction = { id: number; action: BattleAction };
  const savedActions: SavedAction[] = actionIds.map((actionId, index) => ({
    id: Number(actionId),
    action: actionPairs[index].onBattle,
  }));

  const effects = savedActions.flatMap((savedAction) =>
    savedAction.action.effects.map((effect, index) => {
      const actionId = savedAction.id;
      const targetOrder = result.characters.indexOf(effect.target) + 1;
      return { ...effect, actionId, order: index + 1, targetOrder };
    })
  );
  await db.effects.bulkAdd(effects);

  return resultId;
};
export default saveBattleResult;
