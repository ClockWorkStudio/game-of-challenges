import {
  Result as BattleResult,
  Turn as BattleTurn,
  Action as BattleAction,
  Effect as BattleEffect,
  Character as BattleCharacter,
  CharacterState as BattleCharacterState,
} from '../battle/types';
import data from '../data';
import db from '../db';

const loadBattleResult = async (resultId: number): Promise<BattleResult> => {
  const result = await db.results.get(resultId);

  if (result) {
    const characters = await db.characters
      .where('resultId')
      .equals(resultId)
      .sortBy('order');
    const loadBattleCharacters = characters.map(
      async (character): Promise<BattleCharacter> => {
        const acquisitions = await Promise.all(
          await db.acquisitions
            .where('characterId')
            .equals(Number(character.id))
            .toArray()
        );
        const skills = acquisitions.map((a) => {
          const skill = data.skills.find((s) => s.id === a.skillId);
          if (skill) {
            return skill.name;
          }
          throw Error('スキルが見つかりません');
        });
        return { ...character, skills };
      }
    );
    const battleCharacters = await Promise.all(loadBattleCharacters);

    const turns = await db.turns.where('resultId').equals(resultId).toArray();
    const loadBattleTurns = turns.map(async (turn): Promise<BattleTurn> => {
      if (!turn.id) {
        throw Error('ターンIDが取得できません');
      }
      const characterStates = await db.characterStates
        .where('turnId')
        .equals(turn.id)
        .toArray();
      const snapshot: BattleCharacterState[] = characters.map(
        (c) =>
          new Map(
            characterStates
              .filter((state) => state.characterId === c.id)
              .map((state) => [state.kind, state.amount])
          )
      );

      const actions = await Promise.all(
        await db.actions
          .where('turnId')
          .equals(Number(turn.id))
          .toArray()
          .then((value) =>
            value.map(async (action): Promise<BattleAction> => {
              const effects = (
                await db.effects
                  .where('actionId')
                  .equals(Number(action.id))
                  .sortBy('order')
              ).map(
                (effect): BattleEffect => ({
                  ...effect,
                  target: battleCharacters[effect.targetOrder - 1],
                })
              );
              return {
                ...action,
                subject: battleCharacters[action.subjectOrder - 1],
                effects,
              };
            })
          )
      );

      return {
        ...turn,
        actions,
        snapshot,
      };
    });
    const battleTurns = await Promise.all(loadBattleTurns);

    return { ...result, turns: battleTurns, characters: battleCharacters };
  }
  throw Error('データがありません');
};

export default loadBattleResult;
