import { Outlet, Link } from 'react-router-dom';

import React from 'react';

export default function App() {
  return (
    <div>
      <h1>挑戦遊戯</h1>
      <ul>
        <ul>
          <Link to="/">TOP</Link>
        </ul>
        <ul>
          <Link to="/challenges">挑戦する</Link>
        </ul>
      </ul>
      <Outlet />
    </div>
  );
}
