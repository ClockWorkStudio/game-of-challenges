module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  extends: [
    'airbnb',
    'airbnb-typescript',
    'plugin:import/recommended',
    'prettier',
  ],
  parserOptions: { project: './tsconfig.json' },
  rules: {
    'react/jsx-props-no-spreading': ['off'], // react hook formのregister展開を許したい
    'no-unused-vars': ['error', { argsIgnorePattern: '^_' }],
  },
};
